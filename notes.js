const fs = require('fs')
const chalk = require('chalk')

const getNotes = () => "Your notes ..."

/**
 * @param {string} title title for note to add
 * @param {string} body the content of the note
 * @description Adds a new note.
 */
const addNote = (title, body) => {
    const notes = loadNotes() // Get all the previous notes

    // When this returns true, the note is saved into the list
    const duplicateNote = notes.find((note)=> note.title === title)

    debugger

    if(!duplicateNote){

        //Add to the array of notes
        notes.push({
            title: title,
            body: body,
        })

        //Save it
        saveNotes(notes)
        console.log(chalk.green.bold.inverse('New note added!'))
    }
    else{
        console.log(chalk.red.bold.inverse('Note title taken!'))
    }

}

// Remove a specific note based on title
/**
 * @name removeNote
 * @param {string} title title to remove
 * @description  removes a note based on the title
 */
const removeNote = (title) => {
    const notes = loadNotes()
    
    const filteredNotes = notes.filter((note) => note.title !== title )

    if(notes.length > filteredNotes.length){
        saveNotes(filteredNotes)
        console.log(chalk.green.bold.inverse('Note removed!'))
    }
    else{
        console.log(chalk.red.bold.inverse('No note found!'))
    }
}

// Saves the array of notes to the filesystem
const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJSON)
}


const listNotes = () =>{
    console.log(chalk.inverse('Your notes:'))
    const notes = loadNotes()
    notes.forEach( (note) => {
        console.log(note.title)
    })
}

const readNote = (title) =>{
    const notes = loadNotes()
    const foundNote = notes.find( (note) => note.title === title)
    if(foundNote){
        console.log(chalk.inverse('Found Note:'))
        console.log(chalk.bold(foundNote.title))
        console.log(foundNote.body)
    }
    else{
        console.log(chalk.red.bold.inverse('Note not found!'))
    }
}
// returns an array of notes
const loadNotes = () => {
    try{
        const dataBuffer = fs.readFileSync('notes.json') 
        const dataJSON = dataBuffer.toString()
        //console.log(dataJSON)
        return JSON.parse(dataJSON)
    } catch (e){ // base case, the notes.json does not exist
        return []
    }
}

// We are adding properties with defined functions
module.exports = {
    getNotes: getNotes,
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote,
}